export CARGO_INCREMENTAL=0
export RUSTC_BOOSTRAP=1
export RUSTFLAGS="-Zprofile -Copt-level=0 -Clink-dead-code -Zpanic_abort_tests -Cpanic=abort"
export RUSTDOCFLAGS="-Cpanic=abort"
export RUSTUP_TOOLCHAIN="nightly"
export LLVM_PROFILE_FILE="coverage-%p-%m.profraw"
cargo clean --profile dev
cargo test --tests --all-features
grcov . -s . -t html --branch --ignore-not-existing -o ./target/debug/coverage/
