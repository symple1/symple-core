// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Traits for mathematical operations.

pub use core::ops::{
    Index,
    IndexMut,
    ShlAssign,
    ShrAssign,
};

/// Used to add two values and place the result in a mutable value
pub trait Add<Rhs> {
    type Output: ?Sized;
    fn add(&self, rhs: Rhs, out: &mut Self::Output);
}

/// Used to divide two values and find the remainder, placing the results in
/// mutable values
pub trait DivRem<Rhs> {
    type Div: ?Sized;
    type Rem: ?Sized;
    fn div_rem(&self, rhs: Rhs, div: &mut Self::Div, rem: &mut Self::Rem);
}

/// Used to divide a mutable value and return the remainder
pub trait DivRemAssign<Other> {
    type Rem;
    fn div_rem_assign(&mut self, other: Other, rem: &mut Self::Rem);
}

/// Used to multiply two values and place the result in a mutable value
pub trait Mul<Rhs> {
    type Output: ?Sized;
    fn mul(&self, rhs: Rhs, out: &mut Self::Output);
}

/// Used to multiply a mutable value by another value and return any overflow
pub trait MulAssign<Other> {
    type Overflow;
    fn mul_assign(&mut self, other: Other) -> Self::Overflow;
}

/// Used to shift a value left and place the result in a mutable value
pub trait Shl<Rhs> {
    type Output: ?Sized;
    fn shl(&self, rhs: Rhs, out: &mut Self::Output);
}

/// Used to shift a value right and place the result in a mutable value
pub trait Shr<Rhs> {
    type Output: ?Sized;
    fn shr(&self, rhs: Rhs, out: &mut Self::Output);
}

/// Used to subtract two values and place the result in a mutable value,
/// returning `true` if the operation overflowed
pub trait Sub<Rhs> {
    type Output: ?Sized;
    type Overflow;
    fn sub(&self, rhs: Rhs, out: &mut Self::Output) -> Self::Overflow;
}

/// Used to subtract from a mutable value and return any overflow
pub trait SubAssign<Other> {
    type Overflow;
    fn sub_assign(&mut self, other: Other) -> Self::Overflow;
}
