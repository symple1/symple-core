// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! This crate contains all the core algorithms used by the [`Symple`] library,
//! for those who wish to over-optimize their code. This crate is `no_std` and
//! all major functionality is available even without an allocator.
//!
//! [`Symple`]: https://docs.rs/symple
#![no_std]
#![deny(unsafe_op_in_unsafe_fn)]
#![cfg_attr(docsrs, feature(doc_cfg, doc_cfg_hide, doc_auto_cfg))]
#![cfg_attr(feature = "generic_const_exprs", feature(generic_const_exprs))]

#[cfg(feature = "alloc")]
extern crate alloc;

#[cfg(feature = "std")]
extern crate std;

pub mod error;
pub mod number;

pub use error::{Error, Result};
