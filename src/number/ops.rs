// Licensed under the& Apache License, Version 2.0 <LICENSE-APACHE or
// &http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use core::ops::{Deref, DerefMut, Index, IndexMut};
use core::slice::SliceIndex;

use crate::number::{MaybeUninitMut, MaybeUninitTrait};

/// Generic indexing trait
pub trait Get<R>: Index<R> {
    fn get(&self, index: R) -> Option<&Self::Output>;
    unsafe fn get_unchecked(&self, index: R) -> &Self::Output;
}

impl<T, S: SliceIndex<[T]>> Get<S> for [T] {
    fn get(&self, index: S) -> Option<&Self::Output> {
        self.get(index)
    }
    unsafe fn get_unchecked(&self, index: S) -> &Self::Output {
        unsafe { self.get_unchecked(index) }
    }
}

/// Generic mutable indexing trait
pub trait GetMut<R>: Get<R> + IndexMut<R> {
    fn get_mut(&mut self, index: R) -> Option<&mut Self::Output>;
    unsafe fn get_unchecked_mut(&mut self, index: R) -> &mut Self::Output;
}

impl<T, S: SliceIndex<[T]>> GetMut<S> for [T] {
    fn get_mut(&mut self, index: S) -> Option<&mut Self::Output> {
        self.get_mut(index)
    }
    unsafe fn get_unchecked_mut(&mut self, index: S) -> &mut Self::Output {
        unsafe { self.get_unchecked_mut(index) }
    }
}

/// Shift a number left by `other` and place the result into a `MaybeUninit`
/// number, returning the initalized number.
pub trait ShlInto<Other> {
    type MaybeUninitSlice: ?Sized + MaybeUninitMut;
    fn shl_into<M>(&self, other: Other, output: M) -> M::Initialized
    where
        M: MaybeUninitTrait + Deref<Target = Self::MaybeUninitSlice> + DerefMut,
        M::Initialized: Deref<Target = <Self::MaybeUninitSlice as MaybeUninitMut>::Initialized> + DerefMut;
}

/// Shift a number right by `other` and place the result into a `MaybeUninit`
/// number, returning the initalized number.
pub trait ShrInto<Other> {
    type MaybeUninitSlice: ?Sized + MaybeUninitMut;
    fn shr_into<M>(&self, other: Other, output: M) -> M::Initialized
    where
        M: MaybeUninitTrait + Deref<Target = Self::MaybeUninitSlice> + DerefMut,
        M::Initialized: Deref<Target = <Self::MaybeUninitSlice as MaybeUninitMut>::Initialized> + DerefMut;
}

/// Add two numbers and place the result into a `MaybeUninit` number, returning
/// the initalized number.
pub trait AddInto<Other: ?Sized = Self> {
    type MaybeUninitSlice: ?Sized + MaybeUninitMut;
    unsafe fn add_into<M>(&self, other: &Other, output: M) -> (M::Initialized, bool)
    where
        M: MaybeUninitTrait + Deref<Target = Self::MaybeUninitSlice> + DerefMut,
        M::Initialized: Deref<Target = <Self::MaybeUninitSlice as MaybeUninitMut>::Initialized> + DerefMut;
}

/// Subtract `other` from `self` and place the result into a `MaybeUninit`
/// number, returning the initalized number.
pub trait SubInto<Other: ?Sized = Self> {
    type MaybeUninitSlice: ?Sized + MaybeUninitMut;
    unsafe fn sub_into<M>(&self, other: &Other, output: M) -> (M::Initialized, bool)
    where
        M: MaybeUninitTrait + Deref<Target = Self::MaybeUninitSlice> + DerefMut,
        M::Initialized: Deref<Target = <Self::MaybeUninitSlice as MaybeUninitMut>::Initialized> + DerefMut;
}

/// Multiply `self` by `other` and place the result into a `MaybeUninit`
/// number, returning the initalized number.
pub trait MulInto<Other: ?Sized = Self> {
    type MaybeUninitSlice: ?Sized + MaybeUninitMut;
    unsafe fn mul_into<M>(&self, other: &Other, output: M) -> M::Initialized
    where
        M: MaybeUninitTrait + Deref<Target = Self::MaybeUninitSlice> + DerefMut,
        M::Initialized: Deref<Target = <Self::MaybeUninitSlice as MaybeUninitMut>::Initialized> + DerefMut;
}

/// Divide `self` by `other`, returning both the result and the remainder.
pub trait DivRem<Other: ?Sized = Self> {
    type Div;
    type Rem;
    fn div_rem(&self, other: &Other) -> (Self::Div, Self::Rem);
}

/// Divide `self` by `other`, and place the result and remainder into two
/// `MaybeUninit` numbers, returning the initialized numbers.
pub trait DivRemInto<Other: ?Sized = Self> {
    type DivUninitSlice: ?Sized + MaybeUninitMut;
    type RemUninitSlice: ?Sized + MaybeUninitMut;
    unsafe fn div_rem_into<D, R>(&self, other: &Other, div: D, rem: R) -> (D::Initialized, R::Initialized)
    where
        D: MaybeUninitTrait + Deref<Target = Self::DivUninitSlice> + DerefMut,
        D::Initialized: Deref<Target = <Self::DivUninitSlice as MaybeUninitMut>::Initialized> + DerefMut,
        R: MaybeUninitTrait + Deref<Target = Self::RemUninitSlice> + DerefMut,
        R::Initialized: Deref<Target = <Self::RemUninitSlice as MaybeUninitMut>::Initialized> + DerefMut;
}
