// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

#[cfg(feature = "fmt")]
use core::fmt;
use core::hash::Hash;
use core::mem::MaybeUninit;
use core::ops::{Deref, DerefMut};

use crate::number::{Bigit, MaybeUninitTrait};
use crate::number::uint::{UintSlice, MaybeUninitUintSlice, UintArray, MaybeUninitUintArray};

impl<const LEN: usize> MaybeUninitUintArray<LEN> {
    /// Creates a new uninitialized array of [`Bigit`]s
    #[inline]
    #[must_use]
    pub fn uninit() -> Self {
        // TODO: Replace this with the appropriate MaybeUninit function once
        // feature(maybe_uninit_uninit_array) or equivalent is stabilized
        Self(unsafe { MaybeUninit::<[MaybeUninit<Bigit>; LEN]>::uninit().assume_init() })
    }
}

impl<const LEN: usize> MaybeUninitTrait for MaybeUninitUintArray<LEN> {
    type Initialized = UintArray<LEN>;
    /// Extracts the value of the array from the inner `MaybeUninit` containers
    ///
    /// # Safety
    /// All elements of the array must be in an initialized state
    #[inline(always)]
    #[must_use]
    unsafe fn assume_init(self) -> Self::Initialized {
        // SAFETY: guaranteed by caller
        UintArray(self.0.map(|x| unsafe { x.assume_init() }))
    }
}


impl<const LEN: usize> UintArray<LEN> {
    /// Const version of the `Deref` trait
    #[inline(always)]
    #[must_use]
    pub const fn as_slice(&self) -> &UintSlice {
        &UintSlice::from_slice(self.0.as_slice())
    }

    /// Clones the current [`UintArray`] into a new array of given length,
    /// truncating any bigits that don't fit.
    #[inline]
    #[must_use]
    pub fn clone_into<const NEW_LEN: usize>(&self) -> UintArray<NEW_LEN> {
        let mut arr = [0; NEW_LEN];
        arr.iter_mut().zip(self.0.iter()).for_each(|(new, old)| *new = *old);
        UintArray(arr)
    }

    /// Clones the current [`UintArray`] into a new array with additional
    /// length.
    #[cfg(feature = "generic_const_exprs")]
    #[inline]
    #[must_use]
    pub fn clone_and_extend<const ADDITIONAL: usize>(&self) -> UintArray<{LEN + ADDITIONAL}> {
        let mut arr = [0; LEN + ADDITIONAL];
        arr.iter_mut().zip(self.0.iter()).for_each(|(new, old)| *new = *old);
        UintArray(arr)
    }

    /// Const version of the `From<Bigit>` trait
    #[inline]
    #[must_use]
    pub const fn from_bigit(value: Bigit) -> Self
    // where
    //     [(); LEN - 1]: Sized
    {
        let mut arr = [0; LEN];
        arr[0] = value;
        UintArray(arr)
    }

    /// Const version of the `From<[Bigit; LEN]>` trait
    #[inline(always)]
    #[must_use]
    pub const fn from_array(value: [Bigit; LEN]) -> Self {
        UintArray(value)
    }
}

impl<const LEN: usize> From<Bigit> for UintArray<LEN>
    // where
    //     [(); LEN - 1]: Sized
{
    #[inline(always)]
    #[must_use]
    fn from(value: Bigit) -> Self {
        UintArray::from_bigit(value)
    }
}

impl<const LEN: usize> From<[Bigit; LEN]> for UintArray<LEN> {
    #[inline(always)]
    #[must_use]
    fn from(value: [Bigit; LEN]) -> Self {
        UintArray(value)
    }
}

impl<const LEN: usize> Hash for UintArray<LEN> {
    #[inline(always)]
    fn hash<H: core::hash::Hasher>(&self, state: &mut H) {
        self.as_slice().hash(state)
    }
}

impl<const LEN: usize> Eq for UintArray<LEN> {}

impl<const N: usize, U: Deref<Target = UintSlice>> PartialEq<U> for UintArray<N> {
    #[inline(always)]
    #[must_use]
    fn eq(&self, other: &U) -> bool {
        self.as_slice().eq(&*other)
    }
}

impl<const LEN: usize> Ord for UintArray<LEN> {
    #[inline(always)]
    #[must_use]
    fn cmp(&self, other: &Self) -> core::cmp::Ordering {
        self.as_slice().cmp(other.as_slice())
    }
}

impl<const N: usize, U: Deref<Target = UintSlice>> PartialOrd<U> for UintArray<N> {
    #[inline(always)]
    #[must_use]
    fn partial_cmp(&self, other: &U) -> Option<core::cmp::Ordering> {
        self.as_slice().partial_cmp(&*other)
    }
}

#[cfg(feature = "fmt")]
impl<const LEN: usize> fmt::Binary for UintArray<LEN> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.deref().fmt(f)
    }
}

#[cfg(feature = "fmt")]
impl<const LEN: usize> fmt::Octal for UintArray<LEN> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.deref().fmt(f)
    }
}

#[cfg(feature = "fmt")]
impl<const LEN: usize> fmt::LowerHex for UintArray<LEN> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.deref().fmt(f)
    }
}

#[cfg(feature = "fmt")]
impl<const LEN: usize> fmt::UpperHex for UintArray<LEN> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.deref().fmt(f)
    }
}

// macro_rules! array_index_impl { ($sourcetype:ty, $slicetype:ty, $ty:ty) => {
//     impl<const LEN: usize> Index<$ty> for $sourcetype {
//         type Output = $slicetype;
//         #[inline(always)]
//         #[must_use]
//         fn index(&self, index: $ty) -> &Self::Output {
//             <&$slicetype>::from(self.0.index(index))
//         }
//     }

//     impl<const LEN: usize> IndexMut<$ty> for $sourcetype {
//         #[inline(always)]
//         #[must_use]
//         fn index_mut(&mut self, index: $ty) -> &mut Self::Output {
//             <&mut $slicetype>::from(self.0.index_mut(index))
//         }
//     }

//     impl<const LEN: usize> Get<$ty> for $sourcetype {
//         #[inline(always)]
//         #[must_use]
//         fn get(&self, index: $ty) -> Option<&Self::Output> {
//             match self.0.get(index) {
//                 Some(slice) => Some(<&$slicetype>::from(slice)),
//                 None => None,
//             }
//         }

//         #[inline(always)]
//         #[must_use]
//         unsafe fn get_unchecked(&self, index: $ty) -> &Self::Output {
//             // SAFETY: guaranteed by caller
//             <&$slicetype>::from(unsafe { self.0.get_unchecked(index) })
//         }
//     }

//     impl<const LEN: usize> GetMut<$ty> for $sourcetype {
//         #[inline(always)]
//         #[must_use]
//         fn get_mut(&mut self, index: $ty) -> Option<&mut Self::Output> {
//             match self.0.get_mut(index) {
//                 Some(slice) => Some(<&mut $slicetype>::from(slice)),
//                 None => None,
//             }
//         }

//         #[inline(always)]
//         #[must_use]
//         unsafe fn get_unchecked_mut(&mut self, index: $ty) -> &mut Self::Output {
//             // SAFETY: guaranteed by caller
//             <&mut $slicetype>::from(unsafe { self.0.get_unchecked_mut(index) })
//         }
//     }
// } }

macro_rules! array_impl { ($sourcetype:ty, $slicetype:ty, $output:ty) => {
    impl<const LEN: usize> Deref for $sourcetype {
        type Target = $slicetype;
        #[inline(always)]
        #[must_use]
        fn deref(&self) -> &Self::Target {
            self.0.as_slice().into()
        }
    }

    impl<const LEN: usize> DerefMut for $sourcetype {
        #[inline(always)]
        #[must_use]
        fn deref_mut(&mut self) -> &mut Self::Target {
            self.0.as_mut_slice().into()
        }
    }

//     array_index_impl!($sourcetype, $slicetype, (Bound<usize>, Bound<usize>));
//     array_index_impl!($sourcetype, $slicetype, Range<usize>);
//     array_index_impl!($sourcetype, $slicetype, RangeFull);
//     array_index_impl!($sourcetype, $slicetype, RangeFrom<usize>);
//     array_index_impl!($sourcetype, $slicetype, RangeInclusive<usize>);
//     array_index_impl!($sourcetype, $slicetype, RangeTo<usize>);
//     array_index_impl!($sourcetype, $slicetype, RangeToInclusive<usize>);

//     impl<const LEN: usize> Index<usize> for $sourcetype {
//         type Output = $output;
//         #[inline(always)]
//         #[must_use]
//         fn index(&self, index: usize) -> &Self::Output {
//             self.0.index(index)
//         }
//     }

//     impl<const LEN: usize> IndexMut<usize> for $sourcetype {
//         #[inline(always)]
//         #[must_use]
//         fn index_mut(&mut self, index: usize) -> &mut Self::Output {
//             self.0.index_mut(index)
//         }
//     }

//     impl<'a, const LEN: usize> IntoIterator for &'a $sourcetype {
//         type Item = &'a $output;
//         type IntoIter = core::slice::Iter<'a, $output>;
//         #[inline(always)]
//         #[must_use]
//         fn into_iter(self) -> Self::IntoIter {
//             self.0.iter()
//         }
//     }

//     impl<'a, const LEN: usize> IntoIterator for &'a mut $sourcetype {
//         type Item = &'a mut $output;
//         type IntoIter = core::slice::IterMut<'a, $output>;
//         #[inline(always)]
//         #[must_use]
//         fn into_iter(self) -> Self::IntoIter {
//             self.0.iter_mut()
//         }
//     }

//     impl<const LEN: usize> IntoIterator for $sourcetype {
//         type Item = $output;
//         type IntoIter = core::array::IntoIter<$output, LEN>;
//         #[inline(always)]
//         #[must_use]
//         fn into_iter(self) -> Self::IntoIter {
//             self.0.into_iter()
//         }
//     }

//     impl<const LEN: usize> Get<usize> for $sourcetype {
//         #[inline(always)]
//         #[must_use]
//         fn get(&self, index: usize) -> Option<&Self::Output> {
//             self.0.get(index)
//         }

//         #[inline(always)]
//         #[must_use]
//         unsafe fn get_unchecked(&self, index: usize) -> &Self::Output {
//             // SAFETY: guaranteed by caller
//             unsafe { self.0.get_unchecked(index) }
//         }
//     }

//     impl<const LEN: usize> GetMut<usize> for $sourcetype {
//         #[inline(always)]
//         #[must_use]
//         fn get_mut(&mut self, index: usize) -> Option<&mut Self::Output> {
//             self.0.get_mut(index)
//         }

//         #[inline(always)]
//         #[must_use]
//         unsafe fn get_unchecked_mut(&mut self, index: usize) -> &mut Self::Output {
//             // SAFETY: guaranteed by caller
//             unsafe { self.0.get_unchecked_mut(index) }
//         }
//     }
} }

array_impl!(UintArray<LEN>,            UintSlice,            Bigit);
array_impl!(MaybeUninitUintArray<LEN>, MaybeUninitUintSlice, MaybeUninit<Bigit>);