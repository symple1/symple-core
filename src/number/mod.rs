// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! This module is the backend for Symple's [`number`] module.
//!
//! To use this module, first create a number using `UintVec::from` or
//! `UintArray::from`. These structs implement `Deref<Target = UintSlice>`.
//! [`UintSlice`] is the primary integer data type, and [`FloatSlice`] is the
//! primary floating-point data type.
//!
//! # [`Bigit`]
//! Just as every decimal number is composed of digits, bignums are composed of
//! "bigits". For performance reasons, the [`Bigit`] used by this library is a
//! `u128`. In any code that calls this library, the author recommends using the
//! [`Bigit`] and [`HalfBigit`] type aliases (when type declarations are
//! necessary) instead of `u128` or `u64`, because the inner [`Bigit`] type may
//! change. Any change in [`Bigit`] type will cause the major version number of
//! this library to increment.
//!
//! # Integer vs. floating point
//! The integer structs used by this library are "big-endian", meaning the least
//! significant bigit is located at index zero. These structs form the backend
//! for the [`Natural`], [`Integer`], and [`Rational`] numbers of the [`Symple`]
//! library.
//!
//! The floating-point structs used by this library are "little-endian", meaning
//! the most significant bigit is located at index zero. These form the backend
//! for the [`Real`], and [`Complex`] numbers of the [`Symple`] library.
//!
//! # [`MaybeUninit`]
//! Most users will likely be content to let this library (i.e. Rust) allocate
//! and resize their bignums as required, but those with extreme needs may find
//! the `MaybeUninit*` structs useful. Create one of the size you need, then
//! pass it into the function you wish to use. The function will perform the
//! required calculation and return your [`MaybeUninit`] struct, initialized to
//! the desired output value. These functions are `unsafe` because none of them
//! will check to make sure your [`MaybeUninit`] struct is large enough to
//! contain the output value.
//!
//! [`Symple`]: https://docs.rs/symple
//! [`number`]: https://docs.rs/symple/latest/symple/number
//!
//! [`Natural`]: https://docs.rs/symple/latest/symple/number/Natural
//! [`Integer`]: https://docs.rs/symple/latest/symple/number/Integer
//! [`Rational`]: https://docs.rs/symple/latest/symple/number/Rational
//! [`Real`]: https://docs.rs/symple/latest/symple/number/Real
//! [`Complex`]: https://docs.rs/symple/latest/symple/number/Complex
//!
//! [`MaybeUninit`]: https://doc.rust-lang.org/core/mem/union.MaybeUninit.html

use crate::error::{Error, Result};

pub mod ops;
pub mod uint;
// mod float;

pub use uint::{UintSlice, UintArray};
#[cfg(feature = "alloc")]
pub use uint::UintVec;

/// Alias for `u128`, the basic unit of calculation.
pub type Bigit = u128;
/// Alias for `i128`, the signed version of a `Bigit`.
pub type SignedBigit = i128;
/// Alias for `u64`, used in multiplication and division.
pub type HalfBigit = u64;

/// Alias for `u64`, used for << and >> operations.
pub type Shift = u64;
/// The maximum possible shift to not overflow a usize
#[cfg(any(target_pointer_width = "128", target_pointer_width = "64"))]
const MAX_SHIFT: Shift = Shift::MAX;
/// The maximum possible shift to not overflow a usize
#[cfg(not(any(target_pointer_width = "128", target_pointer_width = "64")))]
const MAX_SHIFT: Shift = ((usize::MAX as Shift) + 1) * (Bigit::BITS as Shift) - 1;

/// Smallest f64 greater than 1.
#[cfg(feature = "fmt")]
const FACTOR_OF_SAFETY: f64 = 1.0000000000000002;
/// Log base 10 of 2, used in the Display impls.
#[cfg(feature = "fmt")]
const LOG_10_2: f64 = 0.30102999566398119521373889;
/// Number of power-of-ten [`HalfBigit`]s required to display a number of bits
#[cfg(feature = "fmt")]
const DISPLAY_ALLOC_FACTOR: f64 = FACTOR_OF_SAFETY * LOG_10_2 / (HALF_BIGIT_DIGITS as f64);

/// The number of digits that can fit into a `Bigit`.
#[cfg(feature = "fmt")]
const BIGIT_DIGITS: usize = (LOG_10_2 * Bigit::BITS as f64) as usize;
/// The number of digits that can fit into a `HalfBigit`.
#[cfg(feature = "fmt")]
const HALF_BIGIT_DIGITS: usize = BIGIT_DIGITS/2;
/// The largest power of 10 that fits into a `Bigit`.
#[cfg(feature = "fmt")]
#[allow(dead_code)] // used in uint::slice::display_tests
const BIGIT_TEN: Bigit = (10 as Bigit).pow(BIGIT_DIGITS as u32);
/// The largest power of 10 that fits into a `HalfBigit`.
#[cfg(feature = "fmt")]
const HALF_BIGIT_TEN: HalfBigit = (10 as HalfBigit).pow(HALF_BIGIT_DIGITS as u32);

/// Trait associating an uninitialized slice (e.g.
/// [`uint::MaybeUninitUintSlice`]) with its initialized variant
/// ([`UintSlice`])
pub trait MaybeUninitMut {
    type Initialized: ?Sized;
    unsafe fn assume_init(&mut self) -> &mut Self::Initialized;
}

/// Trait associating an uninitialized struct (e.g.
/// [`uint::MaybeUninitUintArray`]) with its initialized variant
/// ([`UintArray`])
pub trait MaybeUninitTrait {
    type Initialized;
    unsafe fn assume_init(self) -> Self::Initialized;
}

/// Turns a quantity of bits into a quantity of [`Bigit`]s. Equivalent to
/// `bits/Bigit::BITS` rounded up.
#[inline]
#[must_use]
pub const fn bits_to_len(bits: Shift) -> usize {
    let (mut major, minor) = shift_to_parts(bits);
    if minor > 0 {
        major += 1;
    }
    major
}

#[cfg(test)]
#[test]
fn bits_to_bigits_test() {
    assert_eq!(bits_to_len(0), 0);
    assert_eq!(bits_to_len(1), 1);
    assert_eq!(bits_to_len(Bigit::BITS as Shift), 1);
    assert_eq!(bits_to_len(Bigit::BITS as Shift + 1), 2);
    assert_eq!(bits_to_len(Bigit::BITS as Shift * 2), 2);
    assert_eq!(bits_to_len(Bigit::BITS as Shift * 2 + 1), 3);
}

/// Turns an integer bit-shift into the major and minor shifts required.
#[inline(always)]
#[must_use]
pub const fn shift_to_parts(shift: Shift) -> (usize, u32) {
    if shift > MAX_SHIFT { // Prevent overflow
        return (usize::MAX, Bigit::BITS - 1)
    }
    ((shift >> Bigit::BITS.trailing_zeros()) as usize, shift as u32 & (Bigit::BITS - 1))
}

#[cfg(test)]
#[test]
fn shift_to_parts_test() {
    assert_eq!(shift_to_parts(Bigit::BITS as Shift), (1, 0));
    assert_eq!(shift_to_parts(Bigit::BITS as Shift - 1), (0, Bigit::BITS - 1));
    assert_eq!(shift_to_parts(Bigit::BITS as Shift * 3 + 2), (3, 2));
    assert_eq!(shift_to_parts(Bigit::BITS as Shift * 3 - 2), (2, Bigit::BITS - 2));
    #[allow(arithmetic_overflow)]
    if Shift::BITS <= usize::BITS {
        assert_eq!(MAX_SHIFT, Shift::MAX);
        assert_eq!(shift_to_parts(MAX_SHIFT - 1), (MAX_SHIFT as usize/Bigit::BITS as usize, Bigit::BITS - 2));
        assert_eq!(shift_to_parts(MAX_SHIFT), (MAX_SHIFT as usize/Bigit::BITS as usize, Bigit::BITS - 1));
    }
    else {
        assert_eq!(shift_to_parts(MAX_SHIFT - 1), (usize::MAX, Bigit::BITS - 2));
        assert_eq!(shift_to_parts(MAX_SHIFT), (usize::MAX, Bigit::BITS - 1));
        assert_eq!(shift_to_parts(MAX_SHIFT + 1), (usize::MAX, Bigit::BITS - 1));
    }
}

/// Multiplies a [`HalfBigit`] by a [`Bigit`], and returns the widened result.
///
/// Use this function instead of [`widening_mul`] whenever possible; it is much
/// faster.
///
/// This returns the low-order (wrapping) bits and the high-order (overflow)
/// bits of the result as two separate values, in that order.
#[inline(always)]
#[must_use]
pub const fn widening_mul_half(lhs: HalfBigit, rhs: Bigit) -> (Bigit, Bigit) {
    let lhs = lhs as Bigit;

    let mut lower = (rhs & HalfBigit::MAX as Bigit) * lhs;
    let mut upper = (rhs >> HalfBigit::BITS) * lhs;

    let carry;
    (lower, carry) = lower.overflowing_add(upper << HalfBigit::BITS);
    upper >>= HalfBigit::BITS;
    if carry {
        upper += 1;
    }
    (lower, upper)
}

#[cfg(test)]
#[test]
fn widening_mul_half_test() {
    assert_eq!(widening_mul_half(0, 3), (0, 0));
    assert_eq!(widening_mul_half(2, 3), (6, 0));
    assert_eq!(widening_mul_half(HalfBigit::MAX, 2), ((HalfBigit::MAX as Bigit) * 2, 0));
    assert_eq!(widening_mul_half(4, Bigit::MAX), (Bigit::MAX << 2, 3));
    assert_eq!(
        widening_mul_half(HalfBigit::MAX, Bigit::MAX),
        (Bigit::MAX - (HalfBigit::MAX as Bigit) + 1, (HalfBigit::MAX as Bigit) - 1)
    );
}

/// Multiplies the lower- and upper-half of a [`Bigit`] by another [`Bigit`] and
/// returns the widened result.
///
/// This returns the low-order (wrapping) bits and the high-order (overflow)
/// bits of the result as two separate values, in that order.
///
/// All internal multiplication functions use this function instead of
/// [`widening_mul`] because it provides a significant performance gain when
/// used in a loop. The following example comes from the `mul_assign_bigit`
/// function in [`UintSlice`].
/// ```
/// use symple_core::number::{Bigit, HalfBigit, widening_mul_split};
///
/// let mut value = [Bigit::MAX, 3, 2, 1];
/// let other = Bigit::MAX;
///
/// // The following pair of operations would run in every loop iteration if the
/// // loop used `widening_mul`
/// let other_lower = other as HalfBigit;
/// let other_upper = (other >> HalfBigit::BITS) as HalfBigit;
/// let mut carry = 0;
/// for elem in value.iter_mut() {
///     let (lower, upper) = widening_mul_split(other_lower, other_upper, *elem);
///     let add_to_carry;
///     (*elem, add_to_carry) = lower.overflowing_add(carry);
///     carry = upper;
///     if add_to_carry {
///         carry += 1;
///     }
/// }
///
/// assert_eq!(carry, 1);
/// assert_eq!(value, [1, Bigit::MAX-4, 1, 1]);
/// ```
#[inline(always)]
#[must_use]
pub const fn widening_mul_split(lhs_lower: HalfBigit, lhs_upper: HalfBigit, rhs: Bigit) -> (Bigit, Bigit) {
    let lhs_lower = lhs_lower as Bigit;
    let lhs_upper = lhs_upper as Bigit;

    let rhs_lower = rhs & HalfBigit::MAX as Bigit;
    let rhs_upper = rhs >> HalfBigit::BITS;
    let mut first = lhs_lower*rhs_lower;
    let outer     = lhs_lower*rhs_upper;
    let inner     = lhs_upper*rhs_lower;
    let mut last  = lhs_upper*rhs_upper + (outer >> HalfBigit::BITS) + (inner >> HalfBigit::BITS);

    let mut carry;
    (first, carry) = first.overflowing_add(outer << HalfBigit::BITS);
    if carry {
        last += 1;
    }
    (first, carry) = first.overflowing_add(inner << HalfBigit::BITS);
    if carry {
        last += 1;
    }
    (first, last)
}

#[cfg(test)]
#[test]
fn widening_mul_split_test() {
    assert_eq!(widening_mul_split(2, 0, 3), (6, 0));
    assert_eq!(widening_mul_split(1, 2, 3), (3 | (6 << HalfBigit::BITS), 0));
    assert_eq!(widening_mul_split(0, 2, 3 << HalfBigit::BITS), (0, 6));
    assert_eq!(
        widening_mul_split(HalfBigit::MAX, 1, HalfBigit::MAX as Bigit + (1 << HalfBigit::BITS)),
        (1 | (Bigit::MAX << (HalfBigit::BITS + 2)), 3)
    );
    assert_eq!(widening_mul_split(HalfBigit::MAX, HalfBigit::MAX, Bigit::MAX), (1, Bigit::MAX << 1));
}

/// Multiplies two [`Bigit`]s and returns the widened result.
///
/// This returns the low-order (wrapping) bits and the high-order (overflow)
/// bits of the result as two separate values, in that order.
///
/// When used in a loop, [`widening_mul_split`] may be faster.
#[inline(always)]
#[must_use]
pub const fn widening_mul(lhs: Bigit, rhs: Bigit) -> (Bigit, Bigit) {
    widening_mul_split(lhs as HalfBigit, (lhs >> HalfBigit::BITS) as HalfBigit, rhs)
}

#[cfg(test)]
#[test]
fn widening_mul_test() {
    assert_eq!(widening_mul(2, 3), (6, 0));
    assert_eq!(widening_mul(2 << HalfBigit::BITS, 3 << HalfBigit::BITS), (0, 6));
    assert_eq!(widening_mul(Bigit::MAX, Bigit::MAX), (1, Bigit::MAX << 1));
}

/// Attempts to turn a [`Bigit`] into a [`SignedBigit`]
#[inline]
#[must_use]
pub const fn to_signed(negative: bool, bigit: Bigit) -> Result<SignedBigit> {
    const NEG_SIGNEDBIGIT_MIN: Bigit = SignedBigit::MAX as Bigit + 1;
    // Use if-else instead of `match bigit.cmp(&NEG_SIGNEDBIGIT_MIN)` because
    // const fn in traits (and hence in Ord) is unstable at time of writing
    if bigit < NEG_SIGNEDBIGIT_MIN {
        if negative {
            Ok(-(bigit as SignedBigit))
        }
        else {
            Ok(bigit as SignedBigit)
        }
    }
    else if bigit == NEG_SIGNEDBIGIT_MIN {
        if negative {
            Ok(SignedBigit::MIN)
        }
        else {
            Err(Error::TryFromError)
        }
    }
    else {
        Err(Error::TryFromError)
    }
}

#[cfg(test)]
#[test]
fn to_signed_test() {
    assert_eq!(to_signed(true, 0), Ok(0));
    assert_eq!(to_signed(false, 0), Ok(0));
    assert_eq!(to_signed(true, 1), Ok(-1));
    assert_eq!(to_signed(false, 1), Ok(1));
    assert_eq!(to_signed(false, SignedBigit::MAX as Bigit), Ok(SignedBigit::MAX));
    assert_eq!(to_signed(true, SignedBigit::MAX as Bigit), Ok(-SignedBigit::MAX));
    assert_eq!(to_signed(false, SignedBigit::MAX as Bigit + 1), Err(Error::TryFromError));
    assert_eq!(to_signed(true, SignedBigit::MAX as Bigit + 1), Ok(SignedBigit::MIN));
    assert_eq!(to_signed(false, SignedBigit::MAX as Bigit + 2), Err(Error::TryFromError));
    assert_eq!(to_signed(true, SignedBigit::MAX as Bigit + 2), Err(Error::TryFromError));
}

/// Turns a [`SignedBigit`] into its sign and absolute value.
#[inline]
#[must_use]
pub const fn to_unsigned(signed_bigit: SignedBigit) -> (bool, Bigit) {
    (
        signed_bigit < 0,
        if signed_bigit == SignedBigit::MIN {
            SignedBigit::MAX as Bigit + 1 // -SignedBigit::MIN
        }
        else {
            signed_bigit.abs() as Bigit
        }
    )
}

#[cfg(test)]
#[test]
fn to_unsigned_test() {
    assert_eq!(to_unsigned(0), (false, 0));
    assert_eq!(to_unsigned(1), (false, 1));
    assert_eq!(to_unsigned(-1), (true, 1));
    assert_eq!(to_unsigned(SignedBigit::MAX),     (false, SignedBigit::MAX as Bigit));
    assert_eq!(to_unsigned(SignedBigit::MIN + 1), (true,  SignedBigit::MAX as Bigit));
    assert_eq!(to_unsigned(SignedBigit::MIN),     (true,  SignedBigit::MAX as Bigit + 1));
}
